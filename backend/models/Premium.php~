<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "m2k_premium".
 *
 * @property integer $id
 * @property integer $csno
 * @property integer $cdno
 * @property string $rgno
 * @property double $amount
 * @property string $m2k_classname
 * @property integer $benefit
 * @property double $vage
 * @property integer $claim
 * @property integer $driving
 * @property integer $noclaim
 * @property integer $fleet
 * @property integer $restricted
 * @property integer $anttheft
 * @property integer $voluntary
 * @property double $accident
 * @property double $damage
 * @property double $windscreen
 * @property double $towing
 * @property double $strike
 * @property double $accessory
 * @property string $received
 * @property string $user
 * @property integer $approved
 * @property string $approvedby
 * @property string $dateapproved
 * @property string $expirydate
 * @property integer $invoicestatus
 * @property integer $branchId
 *
 * @property M2kCertificate[] $m2kCertificates
 * @property M2kInvoice[] $m2kInvoices
 * @property M2kPayment[] $m2kPayments
 * @property M2kInsurer $cdno0
 * @property M2kCustomer $csno0
 */
class Premium extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm2k_premium';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['csno', 'cdno', 'benefit', 'claim', 'driving', 'noclaim', 'fleet', 'restricted', 'anttheft', 'voluntary', 'approved', 'invoicestatus', 'branchId'], 'integer'],
            [['amount', 'vage', 'accident', 'damage', 'windscreen', 'towing', 'strike', 'accessory'], 'number'],
            [['m2k_classname'], 'required'],
            [['received', 'dateapproved', 'expirydate'], 'safe'],
            [['rgno'], 'string', 'max' => 11],
            [['m2k_classname'], 'string', 'max' => 200],
            [['user', 'approvedby'], 'string', 'max' => 25],
            [['cdno'], 'exist', 'skipOnError' => true, 'targetClass' => Insurer::className(), 'targetAttribute' => ['cdno' => 'm2k_insurerId']],
            [['csno'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['csno' => 'm2k_csno']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'csno' => 'Csno',
            'cdno' => 'Cdno',
            'rgno' => 'Rgno',
            'amount' => 'Amount',
            'm2k_classname' => 'M2k Classname',
            'benefit' => 'Benefit',
            'vage' => 'Vage',
            'claim' => 'Claim',
            'driving' => 'Driving',
            'noclaim' => 'Noclaim',
            'fleet' => 'Fleet',
            'restricted' => 'Restricted',
            'anttheft' => 'Anttheft',
            'voluntary' => 'Voluntary',
            'accident' => 'Accident',
            'damage' => 'Damage',
            'windscreen' => 'Windscreen',
            'towing' => 'Towing',
            'strike' => 'Strike',
            'accessory' => 'Accessory',
            'received' => 'Received',
            'user' => 'User',
            'approved' => 'Approved',
            'approvedby' => 'Approvedby',
            'dateapproved' => 'Dateapproved',
            'expirydate' => 'Expirydate',
            'invoicestatus' => 'Invoicestatus',
            'branchId' => 'Branch ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getM2kCertificates()
    {
        return $this->hasMany(Certificate::className(), ['pId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getM2kInvoices()
    {
        return $this->hasMany(Invoice::className(), ['premiumId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getM2kPayments()
    {
        return $this->hasMany(Payment::className(), ['premiumId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCdno0()
    {
        return $this->hasOne(Insurer::className(), ['m2k_insurerId' => 'cdno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsno0()
    {
        return $this->hasOne(Customer::className(), ['m2k_csno' => 'csno']);
    }
}
