<?php

namespace backend\modules\allocation\controllers;

use yii\web\Controller;

/**
 * Default controller for the `allocation` module
 */
class DefaultController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout = "main_private";

    public function actionIndex() {
        return $this->render('index');
    }

}
