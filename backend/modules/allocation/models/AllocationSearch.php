<?php

namespace backend\modules\allocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\allocation\models\Allocation;

/**
 * AllocationSearch represents the model behind the search form about `backend\modules\allocation\models\Allocation`.
 */
class AllocationSearch extends Allocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_id', 'allocation_batch_id', 'application_id', 'loan_item_id', 'is_canceled'], 'integer'],
            [['allocated_amount'], 'number'],
            [['cancel_comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Allocation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'allocation_id' => $this->allocation_id,
            'allocation_batch_id' => $this->allocation_batch_id,
            'application_id' => $this->application_id,
            'loan_item_id' => $this->loan_item_id,
            'allocated_amount' => $this->allocated_amount,
            'is_canceled' => $this->is_canceled,
        ]);

        $query->andFilterWhere(['like', 'cancel_comment', $this->cancel_comment]);

        return $dataProvider;
    }
}
