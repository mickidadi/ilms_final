<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "programme_fee".
 *
 * @property integer $programme_fee_id
 * @property integer $academic_year_id
 * @property integer $programme_id
 * @property integer $loan_item_id
 * @property double $amount
 * @property integer $days
 * @property integer $year_of_study
 *
 * @property AcademicYear $academicYear
 * @property LoanItem $loanItem
 * @property Programme $programme
 */
class ProgrammeFee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'programme_fee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year_id', 'programme_id', 'loan_item_id', 'days', 'year_of_study'], 'integer'],
            [['amount'], 'number'],
            [['academic_year_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\models\AcademicYear::className(), 'targetAttribute' => ['academic_year_id' => 'academic_year_id']],
            [['loan_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanItem::className(), 'targetAttribute' => ['loan_item_id' => 'loan_item_id']],
            [['programme_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\application\models\Programme::className(), 'targetAttribute' => ['programme_id' => 'programme_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'programme_fee_id' => 'Programme Fee ID',
            'academic_year_id' => 'Academic Year ID',
            'programme_id' => 'Programme ID',
            'loan_item_id' => 'Loan Item ID',
            'amount' => 'Amount',
            'days' => 'Days',
            'year_of_study' => 'Year Of Study',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(\backend\models\AcademicYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanItem()
    {
        return $this->hasOne(LoanItem::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramme()
    {
        return $this->hasOne(\backend\modules\application\models\Programme::className(), ['programme_id' => 'programme_id']);
    }
}
