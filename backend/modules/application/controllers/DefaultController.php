<?php

namespace backend\modules\application\controllers;

use yii\web\Controller;

/**
 * Default controller for the `application` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "main_private";
        return $this->render('index');
    }
}
