<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\application\models\SecurityQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Security Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="security-question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Security Question', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'security_question_id',
            'security_question',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
