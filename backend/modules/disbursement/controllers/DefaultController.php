<?php

namespace backend\modules\disbursement\controllers;

use yii\web\Controller;

/**
 * Default controller for the `disbursement` module
 */
class DefaultController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout = "main_private";

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAllocationBatch() {
        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('allocation-batch', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewbatch($id) {
        return $this->render('allocationbatchprofile', [
                    'model' => $this->findModelbatch($id),
        ]);
    }

    public function actionAllocatedStudent() {
        //$this->layout = "default_main";
        $searchModel = new AllocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('allocatedstudent', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    protected function findModelbatch($id) {
        if (($model = AllocationBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
