<?php

namespace backend\modules\disbursement\models;

use Yii;

/**
 * This is the model class for table "disbursement_batch".
 *
 * @property integer $disbursement_batch_id
 * @property integer $allocation_batch_id
 * @property integer $learning_institution_id
 * @property integer $academic_year_id
 * @property integer $financial_year_id
 * @property integer $instalment_definition_id
 * @property integer $loan_item_id
 * @property integer $batch_number
 * @property string $batch_desc
 * @property integer $instalment_type
 * @property integer $version
 * @property integer $is_approved
 * @property string $approval_comment
 * @property integer $institution_payment_request_id
 * @property string $payment_voucher_number
 * @property string $cheque_number
 * @property string $created_at
 * @property integer $created_by
 *
 * @property Disbursement[] $disbursements
 * @property AcademicYear $academicYear
 * @property AllocationBatch $allocationBatch
 * @property InstalmentDefinition $instalmentDefinition
 * @property InstitutionPaymentRequest $institutionPaymentRequest
 * @property LearningInstitution $learningInstitution
 * @property User $createdBy
 * @property LoanItem $loanItem
 */
class DisbursementBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disbursement_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['allocation_batch_id', 'learning_institution_id', 'academic_year_id', 'financial_year_id', 'instalment_definition_id', 'loan_item_id',  'instalment_type', 'created_at', 'created_by'], 'required'],
            [['allocation_batch_id', 'learning_institution_id', 'academic_year_id', 'financial_year_id', 'instalment_definition_id', 'loan_item_id', 'batch_number', 'instalment_type', 'version', 'is_approved', 'institution_payment_request_id', 'created_by'], 'integer'],
            [['approval_comment'], 'string'],
            [['batch_number','created_at'], 'safe'],
            [['batch_desc'], 'string', 'max' => 100],
            [['payment_voucher_number', 'cheque_number'], 'string', 'max' => 45],
            [['academic_year_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\models\AcademicYear::className(), 'targetAttribute' => ['academic_year_id' => 'academic_year_id']],
            [['allocation_batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\allocation\models\AllocationBatch::className(), 'targetAttribute' => ['allocation_batch_id' => 'allocation_batch_id']],
            [['instalment_definition_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstalmentDefinition::className(), 'targetAttribute' => ['instalment_definition_id' => 'instalment_definition_id']],
            [['institution_payment_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstitutionPaymentRequest::className(), 'targetAttribute' => ['institution_payment_request_id' => 'institution_payment_request_id']],
            [['learning_institution_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\application\models\LearningInstitution::className(), 'targetAttribute' => ['learning_institution_id' => 'learning_institution_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['loan_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\allocation\models\LoanItem::className(), 'targetAttribute' => ['loan_item_id' => 'loan_item_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'disbursement_batch_id' => 'Disbursement Batch',
            'allocation_batch_id' => 'Allocation Batch ID',
            'learning_institution_id' => 'Learning Institution',
            'academic_year_id' => 'Academic Year',
            'financial_year_id' => 'Financial Year',
            'instalment_definition_id' => 'Instalment Definition',
            'loan_item_id' => 'Loan Item',
            'batch_number' => 'Batch Number',
            'batch_desc' => 'Batch Desc',
            'instalment_type' => 'Instalment Type',
            'version' => 'Version',
            'is_approved' => 'Is Approved',
            'approval_comment' => 'Approval Comment',
            'institution_payment_request_id' => 'Institution Payment Request',
            'payment_voucher_number' => 'Payment Voucher Number',
            'cheque_number' => 'Cheque Number',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursements()
    {
        return $this->hasMany(DisbursementBatch::className(), ['disbursement_batch_id' => 'disbursement_batch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(\backend\models\AcademicYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationBatch()
    {
        return $this->hasOne(\backend\modules\allocation\models\AllocationBatch::className(), ['allocation_batch_id' => 'allocation_batch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstalmentDefinition()
    {
        return $this->hasOne(InstalmentDefinition::className(), ['instalment_definition_id' => 'instalment_definition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionPaymentRequest()
    {
        return $this->hasOne(InstitutionPaymentRequest::className(), ['institution_payment_request_id' => 'institution_payment_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearningInstitution()
    {
        return $this->hasOne(\backend\modules\application\models\LearningInstitution::className(), ['learning_institution_id' => 'learning_institution_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanItem()
    {
        return $this->hasOne(\backend\modules\allocation\models\LoanItem::className(), ['loan_item_id' => 'loan_item_id']);
    }
}
