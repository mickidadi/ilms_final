<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\Disbursement;

/**
 * DisbursementSearch represents the model behind the search form about `backend\modules\disbursement\models\Disbursement`.
 */
class DisbursementSearch extends Disbursement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disbursement_id', 'disbursement_batch_id', 'application_id', 'programme_id', 'loan_item_id', 'status', 'created_by'], 'integer'],
            [['disbursed_amount'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Disbursement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'disbursement_id' => $this->disbursement_id,
            'disbursement_batch_id' => $this->disbursement_batch_id,
            'application_id' => $this->application_id,
            'programme_id' => $this->programme_id,
            'loan_item_id' => $this->loan_item_id,
            'disbursed_amount' => $this->disbursed_amount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        return $dataProvider;
    }
}
