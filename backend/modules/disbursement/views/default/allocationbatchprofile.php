<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
/* @var $this yii\web\View */
/* @var $model frontend\models\Fixedassets */

$this->title ="Batch Details";
 
?>
<div class="fixedassets-view">
<div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
<?php
$batchdetails= $this->render('viewbatch', [
                                'model' => $model,
                               
                            ]);
echo TabsX::widget([
    'items' => [
        [
            'label' => 'Allocation Batch Details',
            'content' =>$batchdetails,
            'id' => '1',
        ],
       [
            'label' => 'Allocated Batch Summary',
            'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/default/allocated-student/', 'id' =>$model->allocation_batch_id]) . '" width="100%" height="400px" style="border: 0"></iframe>',
            'id' => '2',
        ],
        [
            'label' => 'Allocated Students',
            'content' => '<iframe src="' . yii\helpers\Url::to(['/disbursement/default/allocated-student', 'id' =>$model->allocation_batch_id]) . '" width="100%" height="400px" style="border: 0"></iframe>',
            'id' => '3',
        ],  
    ],
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' => false
]);
?>
                             </div>
                   
                </div>   