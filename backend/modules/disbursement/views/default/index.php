<?php
$totalallocationbatch = \backend\modules\allocation\models\AllocationBatch::find()->where(['academic_year.is_current' => 1])->joinWith("academicYear")->asArray()->all();
?>  
<div class="disbursement-default-index">
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href=""><span class="info-box-icon bg-aqua"><i class="fa fa-list"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text li">Disbursed Batches</span>
                            <span class="info-box-number"><?= count($totalallocationbatch) ?><small></small></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
