<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-batch-form">
    <?php $form = ActiveForm::begin(); ?>
      <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Allocation Batch:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'allocation_batch_id')->label(false)->dropDownList(
                           ArrayHelper::map(\backend\modules\allocation\models\AllocationBatch::find()->all(),'allocation_batch_id','batch_number'),
                                [
                                'prompt'=>'[--Select Allocation Batch--]',
                                ]
                              ) ?>
                          </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Institution:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'learning_institution_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\modules\application\models\LearningInstitution::find()->all(),'learning_institution_id','institution_name'),
                                [
                                'prompt'=>'[--Select Batch--]',
                                ]
                              ) ?>
                     </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Academic Year:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
   <?= $form->field($model, 'academic_year_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\models\AcademicYear::find()->all(),'academic_year_id','academic_year'),
                                [
                                'prompt'=>'[--Select Academic Year--]',
                                ]
                              ) ?>
                      </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Financial Year:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
   <?= $form->field($model, 'financial_year_id')->label(false)->dropDownList(
                           ArrayHelper::map(\backend\modules\disbursement\models\FinancialYear::find()->all(),'financial_year_id','financial_year'),
                                [
                                'prompt'=>'[--Select Financial Year--]',
                                ]
                              ) ?>
                      </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Instalment :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
   <?= $form->field($model, 'instalment_definition_id')->label(false)->dropDownList(
                           ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::findAll(["is_active"=>1]),'instalment_definition_id','instalment'),
                                [
                                'prompt'=>'[--Select Instalment--]',
                                ]
                              ) ?>
                     </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Loan Item:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
      <?= $form->field($model, 'loan_item_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\modules\allocation\models\LoanItem::findAll(["is_active"=>1]),'loan_item_id','item_name'),
                                [
                                'prompt'=>'[--Select Loan Item--]',
                                ]
                              ) ?>
                      </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Type :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
       <?= $form->field($model, 'instalment_type')->label(false)->dropDownList(
                                 [1=>"Normal",2=>"Additional"],
                                [
                                'prompt'=>'[--Select Instalment Type--]',
                                ]
                              ) ?>
           </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Addition Version:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
       <?= $form->field($model, 'version')->label(false)->dropDownList(
                                [1=>"1",2=>"2",3=>"3",4=>"4",5=>"5",6=>"6"],
                                [
                                'prompt'=>'[--Select Addition Version--]',
                                ]
                              ) ?>
           </div>
                </div>
            </div>
           <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Description:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'batch_desc')->label(false)->textInput(['maxlength' => true]) ?>
 
    <?= $form->field($model, 'created_at')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
    <?= $form->field($model, 'created_by')->label(false)->hiddenInput(["value"=>\yii::$app->user->identity->user_id]) ?>
       </div>
                </div>
            </div>
        </div>
        <div class="space10"></div>
        <div class="col-sm-12">
            <div class="form-group button-wrapper">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>

    </div>
</div>
<div class="space10"></div>