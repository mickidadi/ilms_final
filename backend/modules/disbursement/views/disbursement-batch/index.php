<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disbursement Batches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disbursement Batch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'disbursement_batch_id',
            'allocation_batch_id',
            'learning_institution_id',
            'academic_year_id',
            'instalment_definition_id',
            // 'batch_number',
            // 'batch_desc',
            // 'instalment_type',
            // 'is_approved',
            // 'approval_comment:ntext',
            // 'institution_payment_request_id',
            // 'payment_voucher_number',
            // 'cheque_number',
            // 'created_at',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
