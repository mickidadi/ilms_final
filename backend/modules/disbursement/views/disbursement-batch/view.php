<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementBatch */

$this->title ="View Disbursement Batch";
$this->params['breadcrumbs'][] = ['label' => 'Disbursement Batch', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-batch-view">
 <div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->disbursement_batch_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->disbursement_batch_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'disbursement_batch_id',
            'allocation_batch_id',
            'learning_institution_id',
            'academic_year_id',
            'instalment_definition_id',
            'batch_number',
            'batch_desc',
            'instalment_type',
            'is_approved',
            'approval_comment:ntext',
            'institution_payment_request_id',
            'payment_voucher_number',
            'cheque_number',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
