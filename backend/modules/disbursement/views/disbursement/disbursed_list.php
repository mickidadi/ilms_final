<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\DisbursementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of Disbursement';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disbursement-index">
 <div class="panel panel-info">
                        <div class="panel-heading">
                      <?= Html::encode($this->title) ?>
                        </div>
                        <div class="panel-body">
                            <p>
        <?= Html::a('Add Student', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
                     </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'disbursement_id',
           //'disbursement_batch_id',
            'application_id',
            'programme_id',
            'loan_item_id',
             'disbursed_amount',
             'status',
            // 'created_at',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
 </div>
</div>