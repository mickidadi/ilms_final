<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\Disbursement */

$this->title = 'Update Disbursement: ' . $model->disbursement_id;
$this->params['breadcrumbs'][] = ['label' => 'Disbursements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->disbursement_id, 'url' => ['view', 'id' => $model->disbursement_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disbursement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
