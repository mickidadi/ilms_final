 
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AcademicYear */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academic-year-form">
   
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-sm-8">
       <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
        <div class="profile-info-name">
          <label class="control-label" for="email">Instalment:</label>
        </div>
        <div class="profile-info-value">
    <div class="col-sm-12">
    <?= $form->field($model, 'instalment')->label(false)->textInput(['maxlength' => true]) ?>
    </div>
        </div>
            </div>
            <div class="profile-info-row">
        <div class="profile-info-name">
          <label class="control-label" for="email">Instalment Description:</label>
        </div>
        <div class="profile-info-value">
    <div class="col-sm-12">
    <?= $form->field($model, 'instalment_desc')->label(false)->textInput(['maxlength' => true]) ?>
    </div>
        </div>
            </div>
  <div class="profile-info-row">
        <div class="profile-info-name">
            <label class="control-label" for="email">Status:</label>
        </div>
      <div class="profile-info-value">
    <div class="col-sm-12">
     <?= $form->field($model, 'is_active')->label(false)->dropDownList(
                                [0=>"In Active",1=>'Active'], 
                                [
                                'prompt'=>'[--Select Status--]',
                                ]
                    ) ?> 
       </div>
        </div>
            </div>
       </div>
    <div class="space10"></div>
     <div class="col-sm-12">
    <div class="form-group button-wrapper">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
   <div class="space10"></div>