<?php

namespace backend\modules\repayment\controllers;

use yii\web\Controller;

/**
 * Default controller for the `repayment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout="main_private";
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
