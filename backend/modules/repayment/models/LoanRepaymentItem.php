<?php

namespace backend\modules\repayment\models;

use Yii;

/**
 * This is the model class for table "loan_repayment_item".
 *
 * @property integer $loan_repayment_item_id
 * @property string $item_name
 * @property string $item_code
 * @property integer $is_active
 * @property string $created_at
 * @property integer $created_by
 *
 * @property LoanRepaymentBillDetail[] $loanRepaymentBillDetails
 * @property User $createdBy
 * @property LoanRepaymentSetting[] $loanRepaymentSettings
 */
class LoanRepaymentItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_repayment_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'item_code', 'created_at', 'created_by', 'is_active'], 'required'],
            [['is_active', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['item_name'], 'string', 'max' => 45],
            [['item_code'], 'string', 'max' => 5],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_repayment_item_id' => 'Loan Repayment Item ID',
            'item_name' => 'Item Name',
            'item_code' => 'Item Code',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanRepaymentBillDetails()
    {
        return $this->hasMany(LoanRepaymentBillDetail::className(), ['loan_repayment_item_id' => 'loan_repayment_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanRepaymentSettings()
    {
        return $this->hasMany(LoanRepaymentSetting::className(), ['loan_repayment_item_id' => 'loan_repayment_item_id']);
    }
}
