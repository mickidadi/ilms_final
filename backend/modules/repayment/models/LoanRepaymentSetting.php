<?php

namespace backend\modules\repayment\models;

use Yii;

/**
 * This is the model class for table "loan_repayment_setting".
 *
 * @property integer $loan_repayment_setting_id
 * @property integer $loan_repayment_item_id
 * @property string $start_date
 * @property string $end_date
 * @property double $percent
 * @property string $created_at
 * @property integer $created_by
 *
 * @property LoanRepaymentItem $loanRepaymentItem
 * @property User $createdBy
 */
class LoanRepaymentSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_repayment_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_repayment_item_id', 'percent', 'start_date', 'created_at', 'created_by'], 'required'],
            [['loan_repayment_item_id', 'created_by'], 'integer'],
            [['start_date','end_date', 'created_at'], 'safe'],
            [['percent'], 'number'],
            [['loan_repayment_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanRepaymentItem::className(), 'targetAttribute' => ['loan_repayment_item_id' => 'loan_repayment_item_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_repayment_setting_id' => 'Loan Repayment Setting ID',
            'loan_repayment_item_id' => 'Loan Repayment Item',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'percent' => 'Percent(%)',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanRepaymentItem()
    {
        return $this->hasOne(LoanRepaymentItem::className(), ['loan_repayment_item_id' => 'loan_repayment_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'created_by']);
    }
}
