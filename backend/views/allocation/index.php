<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\AllocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Allocations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allocation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Allocation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'allocation_id',
            'allocation_batch_id',
            'application_id',
            'loan_item_id',
            'allocated_amount',
            // 'is_canceled',
            // 'cancel_comment:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
