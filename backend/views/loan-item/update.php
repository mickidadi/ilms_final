<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\LoanItem */

$this->title = 'Update Loan Item: ' . $model->loan_item_id;
$this->params['breadcrumbs'][] = ['label' => 'Loan Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->loan_item_id, 'url' => ['view', 'id' => $model->loan_item_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loan-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
