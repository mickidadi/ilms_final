<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeFee */

$this->title = 'Create Programme Fee';
$this->params['breadcrumbs'][] = ['label' => 'Programme Fees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programme-fee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
