<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\allocation\models\ProgrammeFeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Programme Fees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programme-fee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Programme Fee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'programme_fee_id',
            'academic_year_id',
            'programme_id',
            'loan_item_id',
            'amount',
            // 'days',
            // 'year_of_study',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
