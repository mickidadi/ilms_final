<?php

use yii\helpers\Html;

$this->title = "Home | ILMS";
?>
<style type="text/css">
    .btn.btn-app{
        min-width:250px;
        min-height:180px;
    }
</style>
<div class="box box-info">
    <div class="box-body">
        <div class="row-fluid">
            <a href="<?= Yii::$app->urlManager->createUrl(['/application/default', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/jobcardassign.png') ?></i>
                <div>Application</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['/allocation/default', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/Logbook.png') ?></i>
                <div>Allocation</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['/disbursement/default', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/transaction12.png') ?></i>
                <div>Disbursement</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['/repayment/default', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/transaction.png') ?></i>
                <div>Repayment</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/lookup', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/Set3.png') ?></i>
                <div>Settings</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/lookup', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/Set3.png') ?></i>
                <div>Users</div>
            </a>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/index', 'id' => 1]) ?>" class="btn btn-app">
                <i><?php echo Html::img('@web/image/Report.png') ?></i>
                <div>Reports</div>
            </a>
        </div>
    </div>
</div>  
