<?php

namespace frontend\modules\disbursement\controllers;

use yii\web\Controller;
use frontend\modules\allocation\models\AllocationBatch;
use frontend\modules\allocation\models\AllocationBatchSearch;
/**
 * Default controller for the `disbursement` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout="main_disbursement";
        return $this->render('index');
    }
     public function actionAllocationBatch()
    {
        $this->layout="main_disbursement"; 
        $searchModel = new AllocationBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('allocationbatch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    
    } 
}
