<?php

namespace frontend\modules\repayment\controllers;

use yii\web\Controller;

/**
 * Default controller for the `repayment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout="main_repayment";
        return $this->render('index');
    }
}
