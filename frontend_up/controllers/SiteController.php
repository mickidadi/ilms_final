<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
       if (!\Yii::$app->user->isGuest) {
         if(1==1){
        return $this->render('index');
         }
         else{
          //enabled
            $this->layout="mainb";
          return $this->render('indexb');
         }
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
         public function actionDashboard()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('dashboard');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
      public function actionPeople()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('people');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
   public function actionChildren()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('children');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
   public function actionLookup()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('lookup');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
      public function actionAttendance()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('attendance');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
    public function actionOffering()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('offering');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
   public function actionGroup()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('group');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
 public function actionClientshome()
    {
        return $this->render('indexclients');
    }
    public function actionMghome()
    {
        return $this->render('indexmg');
    }
    public function actionPshome()
    {
        return $this->render('indexps');
    }
    public function actionChome()
    {
        return $this->render('indexc');
    }
     public function actionNhome($id)
    {
        return $this->render('indexnomotor',["clientId"=>$id]);
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
          $this->layout="main_home";
        if (!Yii::$app->user->isGuest) {
              $this->layout="main_home";
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

     return $this->redirect(['login']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
  public function actionChurch()
    {
       if (!\Yii::$app->user->isGuest) {
        return $this->render('church');
         }  else {
     $this->layout="main_home";
      return $this->redirect(['login']);            
         }
    }
     public function actionImport()
    {
        $model = new \backend\modules\application();

        if ($model->load(Yii::$app->request->post())) {
                   $model->temp= UploadedFile::getInstance($model,'temp');
                 if($model->temp!=""){
                    $model->temp->saveAs('uploadimage/data/'.$model->temp->name);                  
                    $model->temp='uploadimage/data/'.$model->temp->name;
                    $data = \moonland\phpexcel\Excel::widget([
                        'mode' => 'import', 
                        'fileName' => $model->temp, 
                        'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel. 
                        'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric. 
                        //'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
                    ]);
                   //echo count($data);
                   //$data["Equipment"]
                   // print_r($data);
                   //  exit();
                      $room ="";
                 foreach ($data as $datas){
                  //$namesub=$datas["Asset Category"];
            
             /* $result=Yii::$app->db->createCommand("SELECT * FROM lookup_assetsubcategory where name LIKE '%$namesub%' AND categoryId='{$model->groups}' order by id DESC limit 1")->queryAll();
                    //end
                    if(count($result)>0){
                    foreach($result as   $results){

                    }
                    $subId=$results["id"];
                    $assetcategory=$results["codetag"]; 	
                    }*/
                      
                     if(Html::encode($datas["Office_Room"])!=""){
                         $room=Html::encode($datas["Office_Room"]);
                     }
                     //if($datas["Description"]!=""||$datas["C_REPL_COST"]!=""||$datas["DRC_CMV"]!=""){
                     $model1 = new Fixedassets();
                     $searchkey=",";
                        $model1->price=str_replace($searchkey, "", $datas["C_REPL_COST"])==""?0:str_replace($searchkey, "", $datas["C_REPL_COST"]) ;
                        $model1->C_REPL_COST=str_replace($searchkey, "", $datas["C_REPL_COST"])==""?0:str_replace($searchkey, "", $datas["C_REPL_COST"]) ;
                        $model1->DRC_CMV=str_replace($searchkey, "", $datas["DRC_CMV"])==""?0:str_replace($searchkey, "", $datas["DRC_CMV"]) ;
                        $model1->warranty=$model->warranty==""?0:$model->warranty;
                        $model1->assign=$model->assign==""?0:$model->assign[0];
                        $model1->asset_category=Html::encode($datas["Asset Category"]);
                        $model1->file1="no"; 
                        $model1->file2="no";
                        $model1->groups=$model->groups;
                        $model1->powerStation=$model->powerStation;
                        $model1->location=$model->location;
                        $model1->userId=$model->userId;
                       // $model1->sub_group=$subId;
                        $model1->model=Html::encode($datas["Model"]);
                        $model1->make=Html::encode($datas["Make"]);
                        $model1->type=Html::encode($datas["Type"]);
                        $model1->Sizes=Html::encode($datas["Size"]);
                        $model1->serialNo=Html::encode($datas["Serial No"]);
                        $model1->assetName=Html::encode($datas["Description"]);
                           //$model->capacity=$datas["Capacity"];
                        $model1->quantity=Html::encode($datas["Qty"])==""?1:Html::encode($datas["Qty"]);
                        $model1->roomOffice=$room;
                        $model1->old_code=Html::encode($datas["Code"]);
                        $model1->codeNumber=Html::encode($datas["Code"]);
                        $model1->conditions=Html::encode($datas["Condition"]);
                        $model1->uL=Html::encode($datas["UL"])==""?0:Html::encode($datas["UL"]);
                  $model1->save();
                   //  }
                  //print_r($model1);
                 //sexit();
              /*$assetNo=$this->findModel1($subId,$model->location);
             
              $modelocation=\frontend\models\Lookuphydropowerlocation::findone($model->location);
              $costcenter=$modelocation->code;
              $model11 = $this->findModel($model1->id);
              $model11->codeNumber="TAN/".$costcenter."/".$assetcategory."/".$assetNo;
              $model11->save();
              print_r($model1);*/
       
                    } 
                   
                    }
                     //exit();
            return $this->redirect(['fixedassets/index']);
        } else {
            return $this->render('imports', [
                'model' => $model,
            ]);
        }
    }  
 
}
